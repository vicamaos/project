import json
from flask import Flask
from flask import render_template , request # render templates folder templates
from bson.json_util import dumps # Library to convert json
import apirestusers
import apiresttickets

from connection import db

app = Flask(__name__) 

# default
@app.route('/') 
def index():  
   return render_template('index.html') 


@app.route('/users') 
def users():  
   return render_template('pages/users.html')


@app.route('/tickets') 
def tickets():  
   return render_template('pages/tickets.html')

# get the data of the users of the database
@app.route('/v1/users') 
def getUsers(listUsers = []):  
   listUsers = db['Users'].find()  
   return dumps(listUsers)

#get the data of the tickets of the database
@app.route('/v1/tickets') 
def getTickets(listTickets = []):  
   listTickets = db['Interactions'].find()  
   return dumps(listTickets)

#get the data of the tickets  of the api
@app.route('/v1/import/tickets') 
def importTikets(): 
   response = apiresttickets.get_tickets()
   return response

#get the data of the tickets  of the api
@app.route('/v1/import/users') 
def importUsers(): 
   response = apirestusers.get_users()
   return response

@app.route('/v1/create/tickets', methods = ['POST']) 
def createTickets(): 
   data = request.data
   response = apiresttickets.random_tickets(data)
   return response




if __name__ == '__main__':
    app.run( debug=True, port=8000) 
